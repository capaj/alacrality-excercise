import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import ListBooks from './components/ListBooks';
import EditBook from './components/EditBook';
import AddBook from './components/AddBook';
import { apolloClient } from './apollo-client';
import { ApolloProvider } from 'react-apollo';

class App extends Component {
  render() {
    return (
      <ApolloProvider client={apolloClient}>
        <Router>
          <div className="App">
            <header className="App-header">
              <h1 className="App-title">Books app</h1>
              <Link to="/add">Add new</Link>
            </header>
            <section className="Content">
              <Route exact path="/" component={ListBooks} />
              <Route path="/edit/:bookId" component={EditBook} />
              <Route path="/add" component={AddBook} />
            </section>
          </div>
        </Router>
      </ApolloProvider>
    );
  }
}

export default App;
