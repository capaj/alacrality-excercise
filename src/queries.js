import gql from 'graphql-tag';

export const getBooksQuery = gql`
  query {
    books {
      bookId
      title
      author
      price
    }
  }
`;

export const editBookQuery = gql`
  mutation($bookId: Int!, $title: String!, $author: String!, $price: Float!) {
    editBook(bookId: $bookId, title: $title, author: $author, price: $price) {
      bookId
    }
  }
`;
