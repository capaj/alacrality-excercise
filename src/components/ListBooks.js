import React, { Component } from 'react';

import { graphql } from 'react-apollo';
import { Link } from 'react-router-dom';
import { getBooksQuery } from '../queries';
import Loader from './Loader';
import { observer } from 'mobx-react';
import { observable } from 'mobx';

const state = observable({
  count: 0,
  sum: 0
});

const formatCurrency = val => {
  return new Intl.NumberFormat('en-GB', {
    style: 'currency',
    currency: 'GBP'
  }).format(val);
};

@observer
export class ListBooks extends Component {
  render() {
    return (
      <div>
        <div className="SelectedBooks">
          Selected: {state.count} <br /> Total price:{' '}
          {formatCurrency(state.sum)}
        </div>
        <Loader
          render={({ books }) => {
            return books.map(book => {
              const { title, bookId, author, price } = book;

              return (
                <div className="card" key={bookId}>
                  <div className="checkbox-wrapper">
                    {bookId}
                    <input
                      name="selected"
                      type="checkbox"
                      onClick={ev => {
                        if (ev.currentTarget.checked) {
                          state.count++;
                          state.sum += price;
                        } else {
                          state.count--;
                          state.sum -= price;
                        }
                      }}
                    />
                  </div>

                  <div className="content">
                    <h4>
                      <Link to={`/edit/${bookId}`}>{title}</Link>
                    </h4>
                    <div className="detail">
                      <b>{author}</b>
                      <br />
                      Price: <small>{formatCurrency(price)}</small>
                    </div>
                  </div>
                </div>
              );
            });
          }}
          {...this.props}
        />
      </div>
    );
  }
}

export default graphql(getBooksQuery)(ListBooks);
