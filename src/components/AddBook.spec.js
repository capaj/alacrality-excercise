import AddBook from './AddBook';
import React from 'react';
import { shallow } from 'enzyme';

describe('AddBook', function() {
  it('renders', () => {
    const wrapper = shallow(<AddBook />);
    expect(wrapper).toMatchSnapshot();
  });
});
