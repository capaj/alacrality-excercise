import { ListBooks } from './ListBooks';
import React from 'react';
import { shallow } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';

describe('ListBooks', function() {
  it('renders an array of books', () => {
    const wrapper = shallow(
      <MemoryRouter>
        <ListBooks
          data={{
            books: [
              {
                author: 'Dan Vavra',
                title: 'Kingdom come',
                bookId: 1,
                price: 2
              }
            ]
          }}
        />
      </MemoryRouter>,
      { context: { router: {} } }
    );
    expect(wrapper.html()).toMatchSnapshot();
  });
});
