import React, { Component } from 'react';

import { apolloClient } from '../apollo-client';
import gql from 'graphql-tag';
import Bound from 'react-bound';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { Link } from 'react-router-dom';

const state = observable({
  title: '',
  author: '',
  price: 0
});

@observer
class AddBook extends Component {
  render() {
    return (
      <div className="EditView">
        <Bound to={state}>
          <form
            onSubmit={ev => {
              ev.preventDefault();

              apolloClient.mutate({
                mutation: gql`
                  mutation($title: String!, $author: String!, $price: Float!) {
                    createBook(title: $title, author: $author, price: $price) {
                      title
                    }
                  }
                `,
                variables: state
              });
              apolloClient.cache.reset(); // unfortunatelly I didn't find any other way to update the cache
              state.$reset();
              this.props.history.push('/');
            }}
          >
            <label>
              title: <input name="title" />
            </label>
            <label>
              author: <input name="author" />
            </label>
            <label>
              price: <input name="price" type="number" />
            </label>
            <button type="submit">Add</button>
            <Link to="/">Cancel</Link>
          </form>
        </Bound>
      </div>
    );
  }
}

export default AddBook;
