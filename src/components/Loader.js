import React from 'react';

const Loader = ({ data, render }) => {
  if (!data || data.loading) {
    return <div>Loading</div>;
  } else if (data.error) {
    return <div>Loading Failed: {data.error.message}</div>;
  }
  return render(data);
};

export default Loader;
