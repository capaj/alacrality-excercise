import React, { Component } from 'react';

import { apolloClient } from '../apollo-client';
import Bound from 'react-bound';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { editBookQuery, getBooksQuery } from '../queries';
import { Link } from 'react-router-dom';

@observer
class EditBook extends Component {
  @observable book = null;
  async componentDidMount() {
    const query = await apolloClient.query({
      query: getBooksQuery
    });

    const { bookId: currentBookId } = this.props.match.params;
    this.books = query.data.books;
    this.book = query.data.books.find(
      ({ bookId }) => bookId === Number(currentBookId)
    );
  }

  render() {
    if (!this.book) {
      return <div>Loading</div>;
    }
    return (
      <div className="EditView">
        <Bound to={this.book}>
          <form
            onSubmit={async ev => {
              ev.preventDefault();

              await apolloClient.mutate({
                mutation: editBookQuery,
                variables: this.book
              });

              apolloClient.cache.reset(); // unfortunatelly I didn't find any other way to update the cache
              this.props.history.push('/');
            }}
          >
            <label>
              title: <input name="title" />
            </label>
            <label>
              author: <input name="author" />
            </label>
            <label>
              price: <input name="price" type="number" />
            </label>

            <button type="submit">Edit</button>

            <Link to="/">Cancel</Link>
          </form>
        </Bound>
      </div>
    );
  }
}

export default EditBook;
