import test from 'ava';
import puppeteer from 'puppeteer';

test('book app', async t => {
  const browser = await puppeteer.launch({});
  const page = await browser.newPage();
  await page.goto('http://localhost:3000');
  await page.waitForSelector('.Content');
  const delay = time => new Promise(res => setTimeout(() => res(), time));
  await delay(100);
  let links = await page.$$('a');

  t.true(links.length > 19);
  await links[0].click();
  t.is(await page.evaluate('location.pathname'), '/add');

  const [title, author, price] = await page.$$('input');
  await title.focus();
  await title.type('Kingdom come');

  await author.focus();
  await author.type('Dan Vavra');

  await price.focus();
  await price.type('5');

  const addButton = await page.$('button');
  await addButton.click();
  t.is(await page.evaluate('location.pathname'), '/');

  links = await page.$$('a');
  const last = links[links.length - 1];
  const content = await page.evaluate(el => el.innerHTML, last);

  t.is(content, 'Kingdom come');

  // I could easily go on an test the edit too, but I think this should suffice as a demonstration
});
